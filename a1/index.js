let number = 5;

let getCube = (num) => {
    return num ** 3;
}

console.log(`The value of ${number} is ${getCube(number)}`);

const address = ["5-16", "McArthur Highway", "Angeles City", "Philippines", "2009"];

const [lot, street, city, state, zip] = address;

console.log(`I live at ${lot} ${street}. ${city}, ${state} ${zip}.`)

const animal = {
    name: "Brownie",
    type: "dog",
    weight: "8",
    weightType: "lbs",
    footLength: 1,
    inchLength: 3
}

const { name, type, weight, weightType, footLength, inchLength} = animal;

console.log(`${name} is a ${type}. She weights ${weight} ${weightType} with a measurement of ${footLength} ft ${inchLength} in.`);

const arrayNumber = [4, 5, 11, 15, 19];

const optionA = (num) => {
    console.log(num);
}

arrayNumber.forEach(optionA);

const reduceNumber = arrayNumber.reduce((num1, num2) => num1 + num2);

console.log(reduceNumber);

class Dog {
    constructor(name, age, breed) {
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
}

const brownie = new Dog("Brownie", "10", "Shih Tzu");

console.log(brownie);
